import { LitElement, html, css } from "lit";

export class Invoice extends LitElement {
  static styles = [
    css`
      :host {
        display: block;
      }
    `,
  ];

  render() {
    return html`<small>I am the Invoice</small>`;
  }
}
customElements.define("the-invoice", Invoice);
