import { LitElement, html, css } from "lit";
import "./the-invoice";
import "./storage";
export class CecosesolaApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        text-align: center;
      }
    `;
  }

  constructor() {
    super();
    this.title = "Cecosesola App";
  }

  render() {
    return html`
      <div>
        <h1>${this.title}</h1>
        <hr />
      </div>
      <input id="#newitem" placeholder="Code" />
      <button @click=${this.findProduct}>Search</button>
      <div>
        <the-invoice></the-invoice>
      </div>
    `;
  }
  findProduct() {
    return console.log("product-->");
  }
}

customElements.define("cecosesola-test", CecosesolaApp);
